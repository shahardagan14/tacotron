import tensorflow as tf
from tensorflow.keras import layers

from Models.Decoder import Decoder
from Models.Encoder import Encoder
import os

class Tacotron:
    def __init__(self, params):
        self.params = params
        self.training = params.is_training

    def create_model(self):
        """
        Initialize the model and all the networks
        :return:
        """
        print("Initialling the networks....")

        # Create an input layer to use an existing tensor instead of a placeholder
        tokens_layer = layers.Input(shape=[None, ], dtype=tf.int32)
        tokens_lengths = layers.Input(shape=[], dtype=tf.int32)
        mel_layer = layers.Input(shape=[None, self.params.num_mel_bins], dtype=tf.float32)
        mel_length = layers.Input(shape=[], dtype=tf.int32)

        # Initialize the encoder class
        # Encoder is used to encode a variable-length sequence
        # into a fixed-length vector representation
        # Encoder network = Embedding + convolution_layers + LSTM network
        encoder = Encoder(hparams=self.params)

        # Decoder create a mel spectrogram from the encoder outputs
        decoder = Decoder(hparams=self.params)

        if self.training:
            encoder_annotations = encoder(tokens_layer, self.training)  # Output of the LSTMs
            export_pre_mel, export_mels, stop_token, _ = decoder([encoder_annotations, mel_layer], training=True)
            self.training_model = tf.keras.Model(inputs=[tokens_layer, mel_layer], outputs=[export_pre_mel,
                                                                                            export_mels,
                                                                                            stop_token])
            self.training_model.summary()
        else:  # Inference
            encoder_annotations = encoder(tokens_layer, self.training)  # Output of the LSTMs
            _, export_mels, stop_token, alignment = decoder([encoder_annotations, mel_layer])
            self.inference_model = tf.keras.Model(inputs=[mel_layer, tokens_layer],
                                                  outputs=[[export_mels, stop_token, alignment]])
            self.inference_model.summary()

    def save_checkpoint(self):
        path = os.getcwd().join("Checkpoints")
        os.makedirs(path, exist_ok=True)
        self.tacotron.training_model.save_weights(os.path.join(
            path, 'S_{}.CHECKPOINT.H5'.format(self.optimizer.iterations.numpy())))

        print("Checkpoint created!")

    def restore(self, load_step=None):
        path = os.path.join(os.getcwd(), "Checkpoints")
        if load_step is None:
            checkpoint_file_path = tf.train.latest_checkpoint(path)
        else:
            checkpoint_file_path = os.path.join(
                path,
                'S_{}.CHECKPOINT.H5'.format(load_step or self.optimizer.iterations.numpy()))

        if not os.path.exists('{}.index'.format(checkpoint_file_path)):
            print('There is no checkpoint.')
            return

        self.tacotron.training_model.load_weights(checkpoint_file_path)
        print('Checkpoint \'{}\' is loaded.'.format(checkpoint_file_path))
