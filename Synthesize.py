from threading import Thread
from datasets.dataset import inference_input_fn
import datetime
import Tacotron
import os
class Synthesize:
    def __init__(self, params):
        self.params, params
        self.tacotron = Tacotron(params)
        self.tacotron.create_model()

    def Inference(self, sentence_list, label):
        print('Inference running...')

        data = inference_input_fn(sentence_list)
        if data is None:
            print('Inference fail.')
            return
        mels, stops, alignments = self.InferenceStep(
            **data
        )

        exportInferenceThread = Thread(
            target=self.ExportInference,
            args=[
                sentence_list,
                mels.numpy(),
                stops.numpy(),
                alignments.numpy(),
                label or datetime.now().strftime("%Y%m%d.%H%M%S")
            ]
        )
        exportInferenceThread.daemon = True
        exportInferenceThread.start()


    def InferenceStep(self, tokens, token_lengths, initial_mels):
        mel_logits, stop_logits, alignments = self.tacotron.inference_model(
            inputs=[initial_mels, tokens],
            training=False
        )

        return mel_logits, stop_logits, alignments

    def Export_Inference(self, sentence_List, mel_List, stop_List, alignment_List, label):
        pass
        #os.makedirs(os.path.join(self.params., 'Plot').replace("\\", "/"), exist_ok= True)
        #os.makedirs(os.path.join(hp_Dict['Inference_Path'], 'Wav').replace("\\", "/"), exist_ok= True)
