import time
from datetime import datetime

from datasets.dataset import train_input_fn
from Tacotron import Tacotron
import argparse
import os
import tensorflow as tf
import hparams
import numpy as np

"""""
Train module:
Feed  the data to the model
The First task is to clean the dataset and copy the data into a new txt file
1.Consume data from memory
2.Shuffle examples and create mini batches
Opens the given datasets and process the records inside them
"""""
DESCRIPTION = "TTS"


class Train:
    def __init__(self, args):
        self.args = args
        self.params = hparams.hparams()  # Create Instance of the paramaters used
        self.tacotron = Tacotron(self.params)  # Create instance to tacotron class
        self.optimizer = self.create_optimzer()  # Define the optimizer for the model

    def create_optimzer(self):
        """
        :return:
        """
        # Create learning rate as specified in the paper
        learning_Rate = tf.keras.optimizers.schedules.ExponentialDecay(
            initial_learning_rate=self.params.initial_learning_rate,
            decay_steps=50000,
            decay_rate=0.1,
            staircase=True)

        # Create Adam optimizer for the model
        optimizer = tf.keras.optimizers.Adam(
            learning_rate=learning_Rate,
            beta_1=self.params.beta1,
            beta_2=self.params.beta2,
            epsilon=self.params.epsilon)
        return optimizer

    def train_step(self, data):
        """
        Define The loss function and compute the gradients while backpropgation
        :param data: The data of the currnet batch
        :return:
        """
        # Gets the input
        inputs = data['tokens']
        input_lengths = data['tokens_lengths']
        mels = data['mels']
        mels_lengths = data['mels_lengths']

        with tf.GradientTape() as tape:
            pre_mel_logits, mel_logits, stop_logits = self.tacotron.training_model(inputs=[mels, inputs], training=True)

            pre_mel_loss = tf.reduce_mean(tf.abs(mels[:, 1:] - pre_mel_logits), axis=-1)
            mel_loss = tf.reduce_mean(tf.abs(mels[:, 1:] - mel_logits), axis=-1)
            mel_loss += tf.reduce_mean(tf.pow(mels[:, 1:] - mel_logits, 2), axis=-1)

            pre_mel_loss *= tf.sequence_mask(
                lengths=mels_lengths,
                maxlen=tf.shape(mel_loss)[-1],
                dtype=tf.as_dtype(tf.float32)
            )
            mel_loss *= tf.sequence_mask(lengths=mels_lengths,
                                         maxlen=tf.shape(mel_loss)[-1],
                                         dtype=tf.as_dtype(tf.float32))

            # stop > 0.5: Going, stop < 0.5: Done
            stop_loss = tf.nn.sigmoid_cross_entropy_with_logits(
                labels=tf.sequence_mask(
                    lengths=tf.math.ceil(mels_lengths / self.params.step_reduction),
                    maxlen=tf.math.ceil(tf.shape(mel_loss)[-1] / self.params.step_reduction),
                    dtype=tf.as_dtype("float32")), logits=stop_logits)
            loss = tf.reduce_mean(pre_mel_loss) + tf.reduce_mean(mel_loss) + tf.reduce_mean(stop_loss)

        gradients = tape.gradient(loss, self.tacotron.training_model.trainable_variables)
        self.optimizer.apply_gradients(zip(gradients, self.tacotron.training_model.trainable_variables))
        return loss

    def feedforward(self, initial_step=0):

        next_training_batch = train_input_fn(args=self.args, hparams=self.params)

        self.tacotron.create_model()  # Create the training model

        self.optimizer.iterations.assign(initial_step)
        self.tacotron.save_checkpoint()
        # Set up tensorboard
        current_time = datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
        train_log_dir = 'logs/gradient_tape/' + current_time + '/train'
        train_summary_writer = tf.summary.create_file_writer(train_log_dir)
        while True:
            start_Time = time.time()

            loss = self.train_step(next_training_batch)
            if np.isnan(loss):
                raise ValueError('NaN loss')

            with train_summary_writer.as_default():
                tf.summary.scalar('loss', loss)

            display_List = [
                'Time: {:0.3f}'.format(time.time() - start_Time),
                'Step: {}'.format(self.optimizer.iterations.numpy()),
                'LR: {:0.5f}'.format(self.optimizer.lr(self.optimizer.iterations.numpy() - 1)),
                'Loss: {:0.5f}'.format(loss)]
            print('\t\t'.join(display_List))
            if self.optimizer.iterations.numpy() % 1000 == 0:
                self.tacotron.save_checkpoint()



def main():
    parser = argparse.ArgumentParser(description=DESCRIPTION)
    parser.add_argument('--base_dir', default=(os.getcwd()))
    parser.add_argument('--output', default='training')
    parser.add_argument("-t", "--t", help="Train the model", action="store_true")
    args = parser.parse_args()
    train = Train(args)
    train.feedforward()

if __name__ == '__main__':
    main()
