import os

import tensorflow as tf
import numpy as np
from datasets import parse
from datasets import text_normalization
import hparams
import argparse

def inference_input_fn(text, params):
    """
    Prepare the dictionary to feed to the model
    :param params: hparams instance
    :param text: The text we to synthesize
    :return: The dictonary of: tokens ,tokens_length, mels
    """
    pattern_Count = len(text)

    sentence_List = [sentence.lower().strip() for sentence in text]

    token_List = [np.array([text_normalization.chars_index['<S>']] +
                           [text_normalization.chars_index[letter] for letter in sentence] +
                           [text_normalization.chars_index['<E>']], dtype=np.int32)
                  for sentence in sentence_List]

    max_Token_Length = max([token.shape[0] for token in token_List])

    new_Token_Pattern = np.zeros(shape=(pattern_Count, max_Token_Length),
                                 dtype=np.int32
                                 ) + text_normalization.chars_index['<E>']

    new_Initial_Mel_Pattern = np.zeros(
        shape=(pattern_Count, 1, params.num_mel_bins),
        dtype=np.float32
    )

    for pattern_Index, token in enumerate(token_List):
        new_Token_Pattern[pattern_Index, :token.shape[0]] = token

    pattern_Dict = {
        'tokens': new_Token_Pattern,
        'tokens_lengths': np.array([token.shape[0] for token in token_List], dtype=np.int32),
        'mels': new_Initial_Mel_Pattern
    }
    return pattern_Dict


def train_input_fn(args, hparams):
    """
    Prepare a iterator to process data
    :param args: arguments contains directory and options
    :param hparams: The created instance of hparams
    :return: iterator
    """
    input_csv = os.path.join(args.base_dir, 'data', 'LJSpeech-1.1', 'metadata.txt')
    dataset = tf.data.TextLineDataset(input_csv)
    dataset = dataset.map(lambda line: parse.parse_dataset(line, hparams))

    dataset = dataset.repeat(count=50)
    dataset = dataset.shuffle(buffer_size=10000)
    dataset = dataset.padded_batch(batch_size=2, padded_shapes={
        'tokens': [None],
        'mels': [None, hparams.num_mel_bins],
        'tokens_lengths': [],
        'mels_lengths': []}, drop_remainder=False)
    return dataset