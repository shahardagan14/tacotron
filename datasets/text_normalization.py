import re
import inflect
import tensorflow as tf
import numpy as np

letters = 'abcdefghijklmnopqrstuvwxyz'
chars_index = {'<S>': 0, '<E>': 1}
for i in range(len(letters)):
    chars_index[letters[i]] = i + 2

table = tf.lookup.StaticHashTable(
    initializer=tf.lookup.KeyValueTensorInitializer(
        keys=tf.constant(list(chars_index.keys()), dtype=tf.string),
        values=tf.constant(list(chars_index.values()), dtype=tf.int32)), default_value=tf.constant(-1))
# using compile to prepare regex before execution
num_expr = r'\b\d+\b'  # expression for finding numbers in the text
abbreviations = [(r"\b%s\.".format(x[0]), x[1]) for x in
                 [('mrs', 'misess'),
                  ('mr', 'mister'),
                  ('dr', 'doctor'),
                  ('st', 'saint'),
                  ('co', 'company'),
                  ('jr', 'junior'),
                  ('maj', 'major'),
                  ('gen', 'general'),
                  ('drs', 'doctors'),
                  ('rev', 'reverend'),
                  ('lt', 'lieutenant'),
                  ('hon', 'honorable'),
                  ('sgt', 'sergeant'),
                  ('capt', 'captain'),
                  ('esq', 'esquire'),
                  ('ltd', 'limited'),
                  ('col', 'colonel'),
                  ('ft', 'fort')]]


def tokens_to_ID(tokens):
    """
    Create a list of indexes representing each character
    int the text
    :param tokens: String text
    :return: list of the indexes
    """
    tokens = tf.strings.lower(input=tokens, encoding="utf-8")  # convert to lowercase
    for regex, replacement in abbreviations:  # expand abbreviations
        tokens = tf.strings.regex_replace(input=tokens,
                                          pattern=regex,
                                          rewrite=replacement,
                                          replace_global=True)  # all matches

    # remove spaces
    tokens = tf.strings.regex_replace(input=tokens, pattern=" ", rewrite="", replace_global=True)
    text = tf.strings.unicode_split(input=tokens, input_encoding="UTF-8")
    text_idx = table.lookup(text)  # Shape - (T,)
    return text_idx


def text_to_index(text):

    encoded = tf.py_function(lambda x: chars_index[x], [text], tf.int64)
    return encoded
