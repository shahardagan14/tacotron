import tensorflow as tf
import librosa
import os
from datasets.text_normalization import tokens_to_ID, chars_index


def parse_dataset(line, params):
    """
    Preprocesses a single utterance audio/text pair.
    Args:
        line: The line to decode
        params: Hyper parameters
    Returns:
        A
        {inputs:ids, targets:mel_inputs,
        input_sequence_length:int length, target_sequence_lengths:length of mel,
        target_inputs} dict to write to train.txt
    """
    fields = tf.io.decode_csv(records=line,
                              field_delim="|",
                              record_defaults=[[""], [""]],
                              select_cols=[1, 3])
    features = dict(zip(["text", "audio_path"], fields))

    text = features["text"]  # Get the text tensor
    # Convert the tokens to id's
    id_list = tokens_to_ID(text)
    text_idx = tf.convert_to_tensor(value=id_list, dtype=tf.int32)
    # Add start and end token
    start_token = tf.constant([chars_index['<S>']], dtype=tf.int32)
    end_token = tf.constant([chars_index['<E>']], dtype=tf.int32)
    text_idx = tf.concat([start_token, text_idx], axis=0)
    text_idx = tf.concat([text_idx, end_token], axis=0)
    # We also require lengths of every input sequence as inputs to model
    input_sequence_lengths = tf.size(text_idx)  # Tensor with Scalar int32
    path = os.getcwd().join('wavs') + os.sep + features["audio_path"]
    # After We finished to process text We move onto audio
    # STFT configuration values specified in paper
    # Process the wave form representation
    waveform, sample_rate = tf.audio.decode_wav(contents=path,
                                   desired_samples=params.wav_sample_rate,
                                   desired_channels=1)
    transposed = tf.transpose(waveform)
    # trasnpose the matrix and apply stft
    stfts = tf.signal.stft(signals=transposed,
                           frame_length=params.frame_length,
                           frame_step=params.frame_step,
                           fft_length=params.fft_length)

    magnitude_spectrograms = tf.abs(stfts)  # takes the absolutes value
    num_spectrogram_bins = magnitude_spectrograms.shape[-1]

    # These are to be set according to human speech. Values specified in the paper
    lower_edge_hertz, upper_edge_hertz, num_mel_bins = params.lower_edge_hertz, params.upper_edge_hertz, params. \
        num_mel_bins

    linear_to_mel_weight_matrix = tf.signal.linear_to_mel_weight_matrix(num_mel_bins=num_mel_bins,
                                                                        num_spectrogram_bins=num_spectrogram_bins,
                                                                        sample_rate=sample_rate,
                                                                        lower_edge_hertz=lower_edge_hertz,
                                                                        upper_edge_hertz=upper_edge_hertz,
                                                                        dtype=tf.float32)

    mel_spectrograms = tf.tensordot(magnitude_spectrograms, linear_to_mel_weight_matrix, 1)

    mel_spectrograms = tf.squeeze(mel_spectrograms)  # Removes all dimensions that are 1

    # We append a frame of 0s at the end of targets to signal end of target
    end_tensor = tf.tile(input=[[0.0]], multiples=[1, tf.shape(mel_spectrograms)[-1]])
    targets = tf.concat(values=[mel_spectrograms, end_tensor], axis=0)  # Mels with and end token

    # We append a frame of 0s at the start of decoder_inputs to set input at t=1
    start_tensor = tf.tile(input=[[0.0]], multiples=[1, tf.shape(mel_spectrograms)[-1]])
    mels = tf.concat(values=[start_tensor, targets], axis=0)  # Mels with start and ending tokens

    # We require lengths of every target sequence as inputs to model
    mels_sequence_lengths = tf.shape(mels)[0]

    # Return a dict describing this training example:
    return {'tokens': text_idx,
            'mels': mels,
            'tokens_lengths': input_sequence_lengths,
            'mels_lengths': mels_sequence_lengths}
