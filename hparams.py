class hparams:
    def __init__(self):
        self.is_training = True
        self.drop_rate = 0.5
        self.step_reduction = 2
        self.attention_units = 128
        self.initial_learning_rate = 1e-3  # e ^ -5
        self.min_learning_rate = 1e-5
        self.beta1 = 0.9
        self.beta2 = 0.999
        self.epsilon = 1e-7
        # Encoder
        self.encoder_conv_layers = 3  # Number Convolution Layers for the encoder
        self.encoder_conv_kernel_size = 5
        self.encoder_conv_filters = 512
        self.encoder_lstm_units = 256

        # Decoder
        self.decoder_prenet = [256, 256]
        self.decoder_lstm = [1024, 1024]
        self.decoder_postnet_filters = [512, 512, 512]
        self.decoder_kernel_size = [5, 5, 5]
        self.decoder_strides = [1, 1, 1]

        # Audio
        self.wav_sample_rate = 1600
        self.frame_length = 1024
        self.frame_step = 512
        self.fft_length = 1024
        self.num_mel_bins = 80
        self.lower_edge_hertz = 80.0
        self.upper_edge_hertz = 7600.0
        self.preemphasis = 0.97