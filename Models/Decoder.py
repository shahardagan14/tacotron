from tensorflow.keras import Model
from tensorflow.keras import layers
from Models.Models import Conv1D, projection
from Models.Attention import BahdanauMonotonicAttention
import tensorflow as tf

"""
Decoder is an autoregressive recurrent neural network which
predicts a mel spectrogram from the encoded input sequence one
frame at a time
"""


class DecoderStep(tf.keras.Model):

    def __init__(self, hparams):
        super(DecoderStep, self).__init__()
        self.layer = tf.keras.Sequential()
        self.dropout_rate = hparams.drop_rate
        self.prenet_units = hparams.decoder_prenet
        self.attention_units = hparams.attention_units
        self.num_mel_bins = hparams.num_mel_bins
        self.step_reduction = hparams.step_reduction
        self.lstm_layers = hparams.decoder_lstm
        # Build networks
        (self.decoder_prenet, self.attention, self.rnn, self.projection), self.initials = self.build(None)

    def build(self, input_shapes):
        """
        Builds the attention mechanism and the rnn to get
        the initial states early since Monotonic Attention need at
        the the T timestep the t-1 result
        :return: Tuple (attention_mechanism,rnn,[rnn_initial, align_initial])
        """
        # Set Up prenet
        decoder_prenet = Prenet(self.prenet_units, self.dropout_rate)

        # The result of the encoder is a feature embedding matrix of the input which is
        # used as the context for the attention model.
        attention_mechanism = BahdanauMonotonicAttention(size=self.attention_units)

        # Set up decoder rnn and create pointer initials function
        rnn = self.decoder_rnn()

        # Set up projection layer
        decoder_projection = layers.Dense(units=self.num_mel_bins * self.step_reduction + 1)

        # Set up function pointer to initialize initials states
        initial_state = rnn.get_initial_state
        get_initial_alignment = attention_mechanism.initial_alignment_fn

        initials = [initial_state, get_initial_alignment]
        networks = [decoder_prenet, attention_mechanism, rnn, decoder_projection]
        self.built = True
        return networks, initials

    def decoder_rnn(self):
        """
        Stack of 2 uni-directional LSTM layers with 1024 units
        :return: The Stacked RNN Layer
        """
        lstm_cells = [layers.LSTMCell(units=size, recurrent_dropout=0.1)
                      for size in self.lstm_layers]
        # Stack them on top of each other
        decoder_rnn = layers.StackedRNNCells(cells=lstm_cells)
        return decoder_rnn

    def call(self, inputs, training):
        """
        inputs: [encodings, current_mels, previous_alignments, previous_rnn_states]
        encodings: [Batch, T_v, V_dim]
        current_mels: [Batch, Mel_dim]
        previous_alignments: [Batch, T_v]
        previous_rnn_states: A tuple of states
        """
        encodings, mels, previous_alignments, previous_rnn_states = inputs

        prenet = self.decoder_prenet(mels)

        # [Batch, Att_dim], [Batch, T_v]
        attentions, alignments = self.attention([prenet, encodings, previous_alignments])

        # "The prenet output and attention context vector are concatenated
        concatenated_outputs = tf.concat([prenet, attentions], axis=-1)  # [Batch, Prenet_dim + Att_dim]

        #  and passed through a stack of 2 uni-directional LSTM layers"

        new_Tensor, states = self.rnn(concatenated_outputs, states=previous_rnn_states)
        # concatenation of the LSTM output and the attention context
        # vector is projected through a linear transform
        new_Tensor = tf.concat([new_Tensor, attentions], axis=-1)  # [Batch, RNN_dim + Att_dim]
        new_Tensor = self.projection(new_Tensor)  # [Batch, Mel_Dim * r + 1]

        # [Batch, Mel_Dim * r], # [Batch, 1]
        new_Tensor, stops = tf.split(new_Tensor,
                                     num_or_size_splits=[new_Tensor.get_shape()[-1] - 1, 1],
                                     axis=-1)

        return new_Tensor, stops, alignments, states


class Decoder(Model):
    def __init__(self, hparams):
        super(Decoder, self).__init__()
        # Set up Params
        self.params = hparams
        self.decoder_step = DecoderStep(self.params)

    def build(self, input_shapes):
        self.postnet_layer = tf.keras.Sequential()
        self.post_net()
        self.built = True

    def post_net(self):
        """
        The predicted mel spectrogram is passed through a 5-layer convolutional post-net
        which predicts a residual(error is If the exact value is not known)
        to add to the prediction to improve the
        overall reconstruction.
        comprised of 512 filters with shape 5 × 1 with batch
        normalization, followed by tanh
        :return: Improved mel_spectrogram
        """
        activation = 'tanh'
        for index, (filters, kernel_size, stride) in enumerate(zip(
                self.params.decoder_postnet_filters + [self.params.num_mel_bins],
                self.params.decoder_kernel_size + [5],
                self.params.decoder_strides + [1])):
            # The activation of the last layer is not tanh
            if index is len(self.params.decoder_postnet_filters)-1:
                activation = None
            # Stack the layers
            # Build a list of convolution layers
            self.postnet_layer.add(Conv1D(filters=filters,
                                          kernel_size=kernel_size,
                                          activation=activation))
            self.postnet_layer.add(layers.BatchNormalization())
            #23 ו 30

            # During inference only the prenet has dropout
            # Final Projection layer to return to the dimensions of the mel representation
            self.postnet_layer.add(layers.Dropout(rate=self.params.drop_rate))

    def call(self, inputs, training):
        """
        :param inputs:[Encodings, Mels]
        Encodings: [Batch, T_V, V_dim]
        Mels: [Batch, T_q, Mel_dim]
        :return: 
        """

        # Split inputs
        enc_hidden_states, mels = inputs
        # Use last slices of each reduction for training
        mels = mels[:, 0:-1:self.params.step_reduction, :]

        # Get the batch of the encoder and create
        decodings = tf.zeros(
            shape=[tf.shape(enc_hidden_states)[0], 1, self.params.num_mel_bins],
            dtype=enc_hidden_states.dtype
        )
        stops = tf.zeros(
            shape=[tf.shape(enc_hidden_states)[0], 0],
            dtype=enc_hidden_states.dtype
        )  # [Batch, 0]

        # Get the pointer to get initials alignments of attention [Batch, 1, T_v]
        alignments = tf.expand_dims(self.decoder_step.initials[1](
            tf.shape(enc_hidden_states)[0],
            tf.shape(enc_hidden_states)[1],
            enc_hidden_states.dtype), axis=1)

        # rnn state initial
        initial_state = self.decoder_step.initials[0](batch_size=tf.shape(enc_hidden_states)[0],
                                                      dtype=enc_hidden_states.dtype)

        def body(step, decodings, stops, alignments, previous_state):
            # Set assignment to mel_step conditions on training
            mel_step = tf.cond(
                pred=tf.convert_to_tensor(training),
                true_fn=lambda: mels[:, step],
                false_fn=lambda: decodings[:, -1])
            # Get the previous alignments
            previous_alignments = alignments[:, -1]

            # Run Attention and concat outputs
            decoding, stop, alignment, state = self.decoder_step(inputs=[enc_hidden_states,
                                                                         mel_step,
                                                                         previous_alignments,
                                                                         previous_state], training=training)

            decoding = tf.reshape(decoding, shape=[-1, self.params.step_reduction, self.params.num_mel_bins])  # Reshape to 1D
            decodings = tf.concat([decodings, decoding], axis=1)
            stops = tf.concat([stops, stop], axis=-1)
            alignments = tf.concat([alignments, tf.expand_dims(alignment, axis=1)], axis=1)

            return step + 1, decodings, stops, alignments, state

        #
        max_Step = tf.cond(pred=tf.convert_to_tensor(training),
                           true_fn=lambda: tf.shape(mels)[1],
                           false_fn=lambda: 1000 // self.params.step_reduction)
        #
        _, decodings, stops, alignments, _ = tf.while_loop(
            cond=lambda step, decodings, stops, alignments, previous_state: tf.less(step, max_Step),
            body=body,
            loop_vars=[0, decodings, stops, alignments, initial_state],
            shape_invariants=[
                tf.TensorShape([]),
                tf.TensorShape([None, None, self.params.num_mel_bins]),
                tf.TensorShape([None, None]),
                tf.TensorShape([None, None, None]),
                tf.nest.map_structure(lambda x: x.get_shape(), initial_state), ])
        #
        decodings = decodings[:, 1:]
        alignments = alignments[:, 1:]
        #
        post_decodings = self.postnet_layer(decodings) + decodings

        return decodings, post_decodings, stops, alignments


class Prenet(tf.keras.layers.Layer):
    def __init__(self, num_units, dropout_rate):
        super(Prenet, self).__init__()
        self.sizes = num_units
        self.dropout_rate = dropout_rate
        self.layer = tf.keras.Sequential()

    def build(self, input_shape):
        """
        The prenet applies a set of non-linear transformations,
        Contains  2 fully connected layers of 256 hidden ReLU units
        using a bottleneck layer with dropout that helps convergence and improves generalization
        :return: Prenet Model
        """
        rate = self.dropout_rate  # Dropout rate always applied to pre-net
        # layer call action - drawing an arrow from "inputs" to the layer you created
        for units in self.sizes:
            self.layer.add(layers.Dense(units=units, activation='relu'))
            self.layer.add(layers.Dropout(rate=rate))
            # bottleneck layer to improves generalization
        self.built = True

    def call(self, inputs, training):
        return self.layer(inputs=inputs, training=True)  # Always true
