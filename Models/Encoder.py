import tensorflow as tf
from tensorflow.keras import Model
from tensorflow.keras import layers
import Models.Models as m
from datasets.text_normalization import chars_index

class Encoder(Model):
    def __init__(self, hparams):
        super(Encoder, self).__init__()
        self.is_training = hparams.is_training
        self.drop_rate = hparams.drop_rate
        self.conv_kernel_size = hparams.encoder_conv_kernel_size
        self.conv_filters = hparams.encoder_conv_filters
        self.conv_layers = hparams.encoder_conv_layers
        self.lstm_units = hparams.encoder_lstm_units
        self.layer = tf.keras.Sequential()

    def build(self, input_shapes):
        # Create variables for embedding table and initializer
        initializer = tf.random_normal_initializer(stddev=0.5)
        # Set mask to ignore padding using normal initializer to embedding weights
        embedding_layer = layers.Embedding(input_dim=len(chars_index.keys()), output_dim=512,
                                           embeddings_initializer=initializer,
                                           mask_zero=True)
        self.layer.add(embedding_layer)
        # After we pass embedding layer we move to the convolution layers
        self.convolution_layers()
        self.layer.add(self.encoder_rnn())
        self.built = True

    def call(self, inputs, training):
        """
        Feed input into the encoder
        :param inputs: Texts
        :param training:
        :return: The annotations
        """
        return self.layer(inputs, training)

    def convolution_layers(self):
        """
        Pass the embedding through a stack of 3 convolutional layers each
        containing 512 filters with shape 5 × 1
        :return: The 3rd Layer
        """

        for i in range(self.conv_layers):
            self.layer.add(m.Conv1D(filters=self.conv_filters,
                                    kernel_size=self.conv_kernel_size,
                                    activation='relu'))
            self.layer.add(layers.BatchNormalization())
            self.layer.add(layers.Dropout(rate=self.drop_rate))

    def encoder_rnn(self):
        """
        Single bidirectional LSTM layer
        containing 512 units (256 in each direction)
        :return: The forward and backward results are concatenated
        to generate encoded features
        The annotation of the encoder
        (batch_size, max_length, hidden_size)
        """
        # Bidirectional LSTM RNN to extract sequential features from both forward and backward context with zoneout
        # Zoneout is like a selective application of dropout to some of the nodes in a modified computational graph
        # Instead dropping out, units are set to their previous value (h(t) = h(t−1))

        outputs = layers.Bidirectional(tf.keras.layers.LSTM(units=self.lstm_units,
                                                            recurrent_dropout=0.1,
                                                            return_sequences=True))
         # annotations of the encoder
        return outputs