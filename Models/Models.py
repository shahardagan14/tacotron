import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers

""""
Goal: to extract robust sequential representations of text
After we represented the text using the one hot embedded vectors 
We apply a set of non linear transformation("pre-net")
"""""
def projection(num_units, activation):
    """
    Stop Token: Projection to a scalar and through a sigmoid activation or
    Projection layer to r * num_mels dimensions or num_mels dimensions
    :param num_units: Dimensionality of the output space
    :param activation: Activation function to use
    :return: The projected input
    """
    return layers.Dense(units=num_units, activation=activation)



def Conv1D(filters, kernel_size, activation):
    """
    Helper function to save work
    Create a 1D Convolution Layer with the default options
    :param is_training: Are we training the module?
    :param filters: output channels
    :param kernel_size: the size of the kernel
    :param activation: Type of the activation function
    :return: The convolution layer with the specified paramaters
    """
    x = layers.Conv1D(filters=filters, kernel_size=kernel_size, strides=1, padding="same", activation=activation)
    return x



"""
In The end I decided to implement Tacotron 2 instead of Tacotron 1 
Tacotron 2 is the newer version
def cbhg(inputs, k_sets, proj, is_training):

    Convolutional Bank + Highway network + GRU
    The CBHG model Apply K sets of 1-D convolutional filters,
    where the k-th set contains Ck filters of width k
    we use a stride of 1 to preserve the original time resolution
    :param is_training: If we are training the model
    :param proj: array of the output channels of the projections layers
    :param k_sets: number of layers
    :param inputs: The Pre-Net outputs
    :return:  feature embedding for the entirety of the input which
    is used as the context for our attention model.
    conv_layers = []
    for k in range(1, k_sets + 1):
        conv_layers.append(Conv1D(filters=128, kernel_size=k, activation='relu', is_training=is_training))

    # stack the convolution layers on each other
    conv_outputs = layers.concatenate(conv_layers, axis=-1)
    maxpool_outputs = layers.MaxPooling1D(pool_size=2, strides=1, padding='same')(conv_outputs)
    maxpool_outputs = keras.layers.BatchNormalization(maxpool_outputs)

    # 2 Conv1D projections: conv-3-128-ReLU width of 3 and 128 o.channels
    projections = Conv1D(filters=proj[0], kernel_size=3, activation='relu', is_training=is_training)(maxpool_outputs)
    projections = Conv1D(filters=proj[1], kernel_size=3, activation=None, is_training=is_training)(projections)
    # Residual connection
    residual = projections + inputs
    # Highway net - 4 layers of FC-128-ReLU
    highway_net = highway(inputs=residual, units=128)
    for i in range(3):
        highway_net = highway(inputs=highway_net, units=128)

    # After we passed the highway network we go through
    # bidirectional GRU RNN on top to extract sequential features
    # from both forward and backward context
    forward_gru_cell = layers.GRUCell(units=128)
    backward_gru_cell = layers.GRUCell(units=128)
    outputs = layers.Bidirectional(layer=forward_gru_cell, backward_layer=backward_gru_cell, input=highway_net,
                                   merge_mode='concat', dtype=tf.float32)
    return outputs
def highway(inputs, units):
    ""
    Create a layer of relu and sigmoid function based on the paper
    :param inputs: the previous layer
    :param units: number of cells in the layer
    :return: The Highway Layer
    ""
    H = layers.Dense(units=units, activation='relu')(inputs)
    T = layers.Dense(units=units, activation='sigmoid', bias_initializer=tf.constant_initializer(-1.0))(inputs)
    return H * T + inputs * (1.0 - T)
"""
